/*
* Copyright (c) 2008, Willow Garage, Inc.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the Willow Garage, Inc. nor the names of its
*       contributors may be used to endorse or promote products derived from
*       this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

/*
* Copyright (c) 2020 Miller-Tech VIC Pty. Ltd.
* All rights reserved.
* Adapted (poorly) from Willow Garage's *excellent* work at: 
* http://youngsf.cn/indigo/api/tf2_ros/html/c++/transform__listener_8cpp_source.html
*/

#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <getopt.h>

#include <string>
#include <sstream>
#include <iostream>
#include <thread>    
#include <vector>
#include <pqxx/pqxx>
#include <libpq-fe.h>
#include <netinet/in.h>

#include <city.h>

#include <limits>
#include <inttypes.h>

#include <boost/thread/thread.hpp>
#include "libpq-pool/pgbackend.h"

#include <morton-nd/mortonND_BMI2.h>
#include <morton-nd/mortonND_LUT_encoder.h>

#include "std_msgs/Empty.h"
#include "tf2_msgs/TFMessage.h"
#include "ros/callback_queue.h"
 
#include "tf2_ros/buffer.h"
#include <tf2/buffer_core.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "geometry_msgs/Transform.h"
 
#include "boost/thread.hpp"
#include <chrono>

using namespace std::chrono;

using MortonND_8D = mortonnd::MortonNDBmi<8, uint64_t>;

// https://stackoverflow.com/questions/12219928/pack-unpack-short-into-int
int32_t Pack16_to_32(int16_t a, int16_t b)
{
	return (int32_t)( (uint32_t)a<<16 | (uint32_t)b );
}

int16_t UnpackA(int32_t x)
{
   return (int16_t)(((uint32_t)x)>>16);
}

int16_t UnpackB(int32_t x)
{
   return (int16_t)(((uint32_t)x)&0xffff);
}


// https://codereview.stackexchange.com/questions/80386/packing-and-unpacking-two-32-bit-integers-into-an-unsigned-64-bit-integer
uint64_t combine(uint32_t low, uint32_t high)
{
     return (((uint64_t) high) << 32) | ((uint64_t) low);
}

uint32_t high(uint64_t combined)
{
    return combined >> 32;
}

uint32_t low(uint64_t combined)
{
    uint64_t mask = std::numeric_limits<uint32_t>::max();
    return mask & combined; // should I just do "return combined;" which gives same result?
}

// https://stackoverflow.com/questions/22648978/c-how-to-find-the-length-of-an-integer
int int_length(int i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

int int64_length(int64_t i) {
    int l=0;
    for(;i;i/=10) l++;
    return l==0 ? 1 : l;
}

void uint64_to_string( uint64 value, std::string& result ) {
    result.clear();
    result.reserve( 20 ); // max. 20 digits possible
    uint64 q = value;
    do {
        result += "0123456789"[ q % 10 ];
        q /= 10;
    } while ( q );
    std::reverse( result.begin(), result.end() );
}

// https://stackoverflow.com/questions/9695720/how-do-i-convert-a-64bit-integer-to-a-char-array-and-back
void int64ToChar(char mesg[], int64_t num) {
    *(int64_t *)mesg = htonl(num);
}

// https://stackoverflow.com/questions/24538954/how-do-you-cast-a-uint64-to-an-int64
uint64_t Int64_2_UInt64(int64_t value)
{
     return (((uint64_t)((uint32_t)((uint64_t)value >> 32))) << 32) 
        | (uint64_t)((uint32_t)((uint64_t)value & 0x0ffffffff));           
}

int64_t UInt64_2_Int64(uint64_t value)
{
    return (int64_t)((((int64_t)(uint32_t)((uint64_t)value >> 32)) << 32) 
       | (int64_t)((uint32_t)((uint64_t)value & 0x0ffffffff)));           
}

int32_t uint32_to_int32(uint32_t value)
{
	int32_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return value;//tmp;
}

int64_t uint64_to_int64(uint64_t value)
{
	int64_t tmp;
	std::memcpy(&tmp, &value, sizeof(tmp));
	return tmp;
}

char *myhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(int16_t *)dest = htobe16(*(int16_t *)src);
    break;
  case 4:
    *(int32_t *)dest = htobe32(*(int32_t *)src);
    break;
  case 8:
    *(int64_t *)dest = htobe64(*(int64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

char *umyhton(char *src, int size) {
  char *dest = (char *)malloc(sizeof(char) * size);
  switch (size) {
  case 1:
    *dest = *src;
    break;
  case 2:
    *(uint16_t *)dest = htobe16(*(uint16_t *)src);
    break;
  case 4:
    *(uint32_t *)dest = htobe32(*(uint32_t *)src);
    break;
  case 8:
    *(uint64_t *)dest = htobe64(*(uint64_t *)src);
    break;
  default:
    *dest = *src;
    break;
  }
  memcpy(src, dest, size);
  free(dest);
  return src;
}

// https://www.techiedelight.com/get-slice-sub-vector-from-vector-cpp/
template<typename T>
std::vector<T> vec_slice(std::vector<T> const &v, int s, int e)
{
	auto first = v.cbegin() + s;
	auto last = v.cbegin() + e;
	std::vector<T> vec(first, last);
	return vec;
}



 class TransformListener
 {
 
 public:
	
	TransformListener(tf2::BufferCore& buffer, bool spin_thread, std::string &topic_name_, std::string &db_conn_str_, char conninfo[], int &db_pool_size_):
	dedicated_listener_thread_(NULL), buffer_(buffer), using_dedicated_thread_(false), thread_count(db_pool_size_), topic_name(topic_name_)
	{
		if (spin_thread)
			initWithThread();
		else
			init();
		int db_conn_count = db_pool_size_;
		pgbackend = std::make_shared<PGBackend>(db_conn_str_, db_conn_count);
		ros::master::V_TopicInfo topic_infos;
		ros::master::getTopics(topic_infos);
		for(int i, l = topic_infos.size(); i < l; i++) {
			if((topic_infos[i].name).find(topic_name) != std::string::npos)
			{
				type_hash = uint32_to_int32( CityHash32(topic_infos[i].datatype.c_str(), ((size_t)sizeof(topic_infos[i].datatype.size()))) );
			}
		}
	}
	
	TransformListener(tf2::BufferCore& buffer, const ros::NodeHandle& nh, bool spin_thread)
	: dedicated_listener_thread_(NULL)
	, node_(nh)
	, buffer_(buffer)
	, using_dedicated_thread_(false)
	{
		if (spin_thread)
			initWithThread();
		else
			init();
	}
	
	
	~TransformListener()
	{
		using_dedicated_thread_ = false;
		if (dedicated_listener_thread_)
		{
			dedicated_listener_thread_->join();
			delete dedicated_listener_thread_;
		}
	}

 private:
 
	void init()
	{
		message_subscriber_tf_ = node_.subscribe<tf2_msgs::TFMessage>(topic_name, 100, boost::bind(&TransformListener::subscription_callback, this, _1)); 
		message_subscriber_tf_static_ = node_.subscribe<tf2_msgs::TFMessage>(topic_name, 100, boost::bind(&TransformListener::static_subscription_callback, this, _1)); 
	}
	
	void initWithThread()
	{
		using_dedicated_thread_ = true;
		ros::SubscribeOptions ops_tf = ros::SubscribeOptions::create<tf2_msgs::TFMessage>(topic_name, 100, boost::bind(&TransformListener::subscription_callback, this, _1), ros::VoidPtr(), &tf_message_callback_queue_); 
		message_subscriber_tf_ = node_.subscribe(ops_tf);
		
		ros::SubscribeOptions ops_tf_static = ros::SubscribeOptions::create<tf2_msgs::TFMessage>(topic_name, 100, boost::bind(&TransformListener::static_subscription_callback, this, _1), ros::VoidPtr(), &tf_message_callback_queue_); 
		message_subscriber_tf_static_ = node_.subscribe(ops_tf_static);
		
		dedicated_listener_thread_ = new boost::thread(boost::bind(&TransformListener::dedicatedListenerThread, this));
		
		//Tell the buffer we have a dedicated thread to enable timeouts
		buffer_.setUsingDedicatedThread(true);
	}
	
	
	
	void subscription_callback(const ros::MessageEvent<tf2_msgs::TFMessage const>& msg_evt)
	{
		subscription_callback_impl(msg_evt, false);
	}
	void static_subscription_callback(const ros::MessageEvent<tf2_msgs::TFMessage const>& msg_evt)
	{
		subscription_callback_impl(msg_evt, true);
	}
	
	void subscription_callback_impl(const ros::MessageEvent<tf2_msgs::TFMessage const>& msg_evt, bool is_static)
	{
		// ROS_INFO("callBack triggered");
		ros::Time now = ros::Time::now();
		if(now < last_update_){
			ROS_WARN_STREAM("Detected jump back in time of " << (last_update_ - now).toSec() << "s. Clearing TF buffer.");
			buffer_.clear();
		}
		last_update_ = now;		
		
		const tf2_msgs::TFMessage& msg_in = *(msg_evt.getConstMessage());
		
		high_resolution_clock::time_point t1 = high_resolution_clock::now();
		this->persistDescriptors(msg_in);		
		high_resolution_clock::time_point t4 = high_resolution_clock::now();

		auto transaction_total_duration = duration_cast<milliseconds>( t4 - t1 ).count();
		ROS_INFO("transaction_total_duration: %i", transaction_total_duration);
	}
	
	void persistDescriptors(tf2_msgs::TFMessage msg_in)
	{
		/* Binary COPY demo */
		char header[12] = "PGCOPY\n\377\r\n\0";
		int buf_pos = 0;
		int flag = 0;
		int extension = 0;
		int row_count = msg_in.transforms.size();

		int buffer_size = (((row_count) * 30)+21);

		char buffer[buffer_size];
		int size = 0;
		buf_pos += 11;

		memcpy(buffer, header, buf_pos);		
		memcpy(&buffer[buf_pos], (void *)&flag, 4);
		buf_pos += 4;

		memcpy(&buffer[buf_pos], (void *)&extension, 4);
		buf_pos += 4;

		
		short fieldnum = 0;
		int32_t id = 0;
		int64_t mc = 0;
		int64_t val = 0;
		int oid_length = 4;
		int oid = 0;
		int desc_idx = 0;
		char buff[21];

		for(unsigned int i = 0, l = row_count; i < l; i++) 
		{

			fieldnum = 3;
			memcpy(&buffer[buf_pos], myhton((char *)&fieldnum, 2), 2);
			buf_pos += 2;

			size = 8;
			memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			buf_pos += 4;

			double raw_val = msg_in.transforms[i].transform.rotation.x;
			mc = 0;
			if(raw_val != 0)
			{
				std::string str_defloat = boost::lexical_cast<std::string>(raw_val);
				str_defloat = str_defloat.substr(0,str_defloat.find("e-")).replace(str_defloat.find("."),1,"");
				std::istringstream iss(str_defloat);
				iss >> mc;				
			}
			
			memcpy(&buffer[buf_pos], myhton((char *)&mc, 8), 8);
			buf_pos += 8;

			// type
			size = 4;
			memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			buf_pos += 4;

			memcpy(&buffer[buf_pos], &type_hash, 4);
			buf_pos += 4;

			// id: hash of x + y
			size = 4;
			memcpy(&buffer[buf_pos], myhton((char *)&size, 4), 4);
			buf_pos += 4;

			id = uint32_to_int32( CityHash32( msg_in.transforms[i].child_frame_id.c_str(), msg_in.transforms[i].child_frame_id.size()) );
			
			memcpy(&buffer[buf_pos], myhton((char *)&id, 4), 4);
			buf_pos += 4;

		}

	// 	// end of file
		short negative = -1;
		memcpy(&buffer[buf_pos], myhton((char *)&negative, 2), 2);
		
		auto conn = pgbackend->connection();
		PGresult *res = NULL;

	// 	// char *frame_time;
	// 	// int frame_time_length = 0;
	// 	// res = PQexec(conn->connection().get(), "select now()::text;");
	// 	// if (PQresultStatus(res) != PGRES_TUPLES_OK) {
	// 	// 	std::cout << "Select failed: " << PQresultErrorMessage(res) << std::endl;
	// 	// } else {
	// 	// 	// http://www.cplusplus.com/reference/cstdlib/atoll/
	// 	// 	frame_time = PQgetvalue(res, 0, 0);
	// 	// 	frame_time_length = PQgetlength(res, 0, 0);
	// 	// }
	// 	// std::cout << "frame_time length: " << std::to_string(PQgetlength(res, 0, 0)) << std::endl;
	// 	// PQclear(res);

		res = PQexec(conn->connection().get(), "COPY mem_l0 (v, t, i) FROM STDIN (FORMAT binary);");
		if (PQresultStatus(res) != PGRES_COPY_IN)
		{
			std::cout << "Not in COPY_IN mode";
			PQclear(res);
		}
		else
		{
			PQclear(res);
			int copyRes = PQputCopyData(conn->connection().get(), buffer, buffer_size);
			if (copyRes == 1)
			{
				if (PQputCopyEnd(conn->connection().get(), NULL) == 1)
				{
					res = PQgetResult(conn->connection().get());
					if (PQresultStatus(res) != PGRES_COMMAND_OK)
					{
						std::cout << PQerrorMessage(conn->connection().get()) << std::endl;
					}
					PQclear(res);
				}
				else
				{
					std::cout << PQerrorMessage(conn->connection().get()) << std::endl;
				}
			}
			else if (copyRes == 0)
			{
				std::cout << "Send no data, connection is in nonblocking mode" << std::endl;
			}
			else if (copyRes == -1)
			{
				std::cout << "Error occur: " << PQerrorMessage(conn->connection().get()) << std::endl;
			}
		}
		pgbackend->freeConnection(conn);
	}

	ros::CallbackQueue tf_message_callback_queue_;
	boost::thread* dedicated_listener_thread_;
	ros::NodeHandle node_;
	ros::Subscriber message_subscriber_tf_;
	ros::Subscriber message_subscriber_tf_static_;
	tf2::BufferCore& buffer_;
	bool using_dedicated_thread_;
	ros::Time last_update_;
	std::string topic_name;

	void dedicatedListenerThread()
	{
		while (using_dedicated_thread_)
		{
		tf_message_callback_queue_.callAvailable(ros::WallDuration(0.01));
		}
	};

	int thread_count = 0;

	int32_t type_hash = 0;

	std::shared_ptr<PGBackend> pgbackend;

 };
 
 
 
 
 
int main(int argc, char **argv)
{
	int thread_count = 4;
	int tick_rate = 50;
	int opt = 0;

	std::string topicName = ""; 

	std::string postgresHostIP = "";
	std::string postgresPort = "5432";
	std::string postgresDBName = "postgres";
	std::string postgresUser = "postgres";
	std::string postgresPassword = "password";

	std::string container_id = "";

	const char *opts = "+"; // set "posixly-correct" mode
	const option longopts[]{
		{"postgresHostIP", 1, 0, 'a'},
		{"postgresPort", 1, 0, 'b'},
		{"postgresDBName", 1, 0, 'c'},
		{"postgresUser", 1, 0, 'd'},
		{"postgresPassword", 1, 0, 'e'},
		{"containerId", 1, 0, 'f'},
		{"topicName", 1, 0, 'g'},
		{"thread_count", 1, 0, 'h'},
		{"tick_rate", 1, 0, 'i'},
		{0, 0, 0, 0}}; 

	while ((opt = getopt_long_only(argc, argv, opts, longopts, 0)) != -1)
	{
		switch (opt)
		{
		case 'a':
			std::cout << "setting postgresHostIP: " << optarg << std::endl;
			postgresHostIP = optarg;
			break;
		case 'b':
			std::cout << "setting postgresPort: " << optarg << std::endl;
			postgresPort = optarg;
			break;
		case 'c':
			std::cout << "setting postgresDBName: " << optarg << std::endl;
			postgresDBName = optarg;
			break;
		case 'd':
			std::cout << "setting postgresUser: " << optarg << std::endl;
			postgresUser = optarg;
			break;
		case 'e':
			std::cout << "setting postgresPassword: " << optarg << std::endl;
			postgresPassword = optarg;
			break;
		case 'f':
			std::cout << "setting container id: " << optarg << std::endl;
			container_id = optarg;
			break;
		case 'g':
			std::cout << "setting topicName: " << optarg << std::endl;
			topicName = optarg;
			break;
		case 'h':
			std::cout << "setting thread count: " << optarg << std::endl;
			thread_count = atoi(optarg);
			break;
		case 'i':
			std::cout << "setting tick Hz rate: " << optarg << std::endl;
			tick_rate = atoi(optarg);
			break;
		default: 
			exit(EXIT_FAILURE);
		}
	}

	std::cout << "Starting..." << std::endl;

	std::string ros_node_name = "TF2_" + container_id;

	ros::init(argc, argv, ros_node_name);
	ros::NodeHandle nh;

	ros::master::V_TopicInfo topic_infos;
  	ros::master::getTopics(topic_infos);
	std::cout << "ROS topics: " << std::endl;
	for(int i, l = topic_infos.size(); i < l; i++) {
		std::cout << "topic name: " << topic_infos[i].name << " topic type: " << topic_infos[i].datatype << std::endl;
	}
	// defaults to Unix socket - 30% faster than TCP
	std::string db_conn_str = "dbname='" + postgresDBName + "' user='" + postgresUser + "' password='" + postgresPassword + "'";
	// only if host IP specified then switch to TCP
	if(postgresHostIP.length() > 0)
	{
		db_conn_str = "postgresql://" + postgresUser + ":" + postgresPassword + "@" + postgresHostIP + ":" + postgresPort + "/" + postgresDBName;
	}
	std::cout << db_conn_str << std::endl;
	char conninfo[db_conn_str.size() + 1];
	strcpy(conninfo, db_conn_str.c_str());
	tf2::BufferCore buffer(ros::Duration(0.5));
	bool dedicate_thread = true;
	boost::shared_ptr<TransformListener> tl_ptr;
	tl_ptr.reset(new TransformListener(buffer, dedicate_thread, topicName, db_conn_str, conninfo, thread_count));
	
	ros::Rate rate(tick_rate);
	while (ros::ok())
	{
		ros::spinOnce();
		rate.sleep();
	}

	return 0;
}
