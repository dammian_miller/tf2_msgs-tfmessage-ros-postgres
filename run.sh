#!/bin/bash
set -e
echo "export CONTAINER_ID=`head -1 /proc/self/cgroup|cut -d/ -f3`" >> /root/.bashrc
source /opt/ros/kinetic/setup.bash
sleep ${START_DELAY_WAIT:-0}
./build/devel/lib/tf2_msgs-tfmessage-ros-postgres/tf2_msgs-tfmessage-ros-postgres --topicName=${TOPIC_NAME:-image} --containerId=${CONTAINER_ID:-1} --thread_count=${THREAD_COUNT:-3} --postgresHostIP=${POSTGRES_HOST_IP:-} --postgresPort=${POSTGRES_PORT:-} --postgresUser=${POSTGRES_USER:-postgres} --postgresPassword=${POSTGRES_PASSWORD:-password} --postgresDBName=${POSTGRES_DB_NAME:-postgres} --tick_rate=${TICK_RATE:-50}
